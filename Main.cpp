#include <iostream>
const int N = 33;

void FindEvenNumbers()
{
	int a = N;
	for (int i = 1; i <= a;)
	{
		while (i % 2 == 0)
		{
			std::cout << i << "\n";
			break;
		}
		i++;
	}
}

int main()
{
	std::cout << "Even numbers between 0 and " << N << " are \n";
	FindEvenNumbers();
}